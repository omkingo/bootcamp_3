package com.omkingo.bootcamp.basic_api.ui

import com.nhaarman.mockitokotlin2.*
import com.omkingo.bootcamp.basic_api.data.BeerModel
import com.omkingo.bootcamp.basic_api.service.BeerApiService
import org.junit.Test

import org.junit.Before
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerPresenterTest {

    @Mock
    lateinit var testView: BeerInterface

    @Mock
    lateinit var service: BeerApiService

    lateinit var presenter: BeerPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = BeerPresenter(testView, service)
    }

    @Test
    fun testGetRandomBeerApi() {
        //given
        whenever(service.getRandomBeer()).thenReturn(mock())

        //when
        presenter.getBeerApi()

        //then
        verify(service).getRandomBeer()
    }

    @Test
    fun testGetBeerApiFailed() {
        //given
        val call = mock<Call<List<BeerModel>>>()
        whenever(service.getRandomBeer()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<BeerModel>>>(0).onFailure(mock(), mock())
        }

        //when
        presenter.getBeerApi()

        //then
        verify(testView).showErrorMsg(anyString())
        verifyNoMoreInteractions(testView)
    }

    @Test
    fun testGetBeerApiResponseBodyNull() {
        //given
        val call = mock<Call<List<BeerModel>>>()
        whenever(service.getRandomBeer()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<BeerModel>>>(0).onResponse(mock(), Response.success(null))
        }

        //when
        presenter.getBeerApi()

        //then
        verifyZeroInteractions(testView)
    }

    @Test
    fun testGetBeerApiResponseBodyEmptyList() {
        //given
        val call = mock<Call<List<BeerModel>>>()
        whenever(service.getRandomBeer()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<BeerModel>>>(0).onResponse(mock(), Response.success(listOf()))
        }

        //when
        presenter.getBeerApi()

        //then
        verifyZeroInteractions(testView)
    }

    @Test
    fun testGetBeerApiResponseBodyBeerModel() {
        //given
        val beerModel = BeerModel("name", "des", 1.0, "image")
        val call = mock<Call<List<BeerModel>>>()
        whenever(service.getRandomBeer()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<BeerModel>>>(0).onResponse(mock(), Response.success(listOf(beerModel)))
        }

        //when
        presenter.getBeerApi()

        //then
        verify(testView).setBeer(eq(beerModel))
    }
}