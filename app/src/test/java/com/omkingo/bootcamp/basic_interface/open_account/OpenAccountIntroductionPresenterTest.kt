package com.omkingo.bootcamp.basic_interface.open_account

import com.omkingo.bootcamp.basic_interface.IntroductionInterface
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.anyString
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class OpenAccountIntroductionPresenterTest {

    @Mock
    lateinit var testView: IntroductionInterface

    @InjectMocks
    lateinit var presenter: OpenAccountIntroductionPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun init() {
        presenter.init()

        verify(testView).setContent(anyString())
        verify(testView).setContent("Open Account\n Term and Conditions")
    }
}