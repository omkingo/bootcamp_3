package com.omkingo.bootcamp.basic_abstract.debit_card

import com.omkingo.bootcamp.basic_abstract.AbsIntroductionActivity

class DebitCardIntroduction : AbsIntroductionActivity() {

    override fun navigateNext() {
        //Navigate to next screen
    }

    override fun setContent(contentText: String) {
        super.setContent("hello Debit")
        println("this is Debit")
    }
}