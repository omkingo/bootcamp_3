package com.omkingo.bootcamp.basic_api.service

import com.omkingo.bootcamp.basic_api.data.BeerModel
import retrofit2.Call
import retrofit2.http.GET

interface BeerApiService {

    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>
}