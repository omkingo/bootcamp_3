package com.omkingo.bootcamp.basic_api.data

import com.google.gson.annotations.SerializedName

data class BeerModel(
    @SerializedName("name")
    var name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("abv")
    val abv: Double?,

    @SerializedName("image_url")
    val imageUrl: String?
)