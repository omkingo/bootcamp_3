package com.omkingo.bootcamp.basic_api.ui

import com.omkingo.bootcamp.basic_api.data.BeerModel
import com.omkingo.bootcamp.basic_api.service.BeerApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerPresenter(val view: BeerInterface, private val service: BeerApiService) {

    fun getBeerApi() {
        service.getRandomBeer().enqueue(object : Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                view.showErrorMsg("API FAILED")
            }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }
        })
    }
}
