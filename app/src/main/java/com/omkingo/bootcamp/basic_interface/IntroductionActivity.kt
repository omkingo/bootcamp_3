package com.omkingo.bootcamp.basic_interface

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.todo.model.ToDoModel
import kotlinx.android.synthetic.main.introduction.introductionContent

class IntroductionActivity : AppCompatActivity(), IntroductionInterface {

    companion object {
        const val EXTRA_KEY_TITLE = "TITLE_NAME_INTRO"
        const val EXTRA_KEY_MODEL = "MODEL"
        fun startActivity(context: Context, titleName: String) =
            context.startActivity(
                Intent(context, IntroductionActivity::class.java).also { myIntent ->
                    myIntent.putExtra(EXTRA_KEY_TITLE, titleName)
//                    myIntent.putExtra(EXTRA_KEY_MODEL, model)
                })
    }

    private val presenter = IntroductionPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.introduction)
        presenter.init()
    }

    override fun setContent(contentText: String) {
        //Set ui content
        introductionContent.text = contentText
    }

    override fun navigateNext() {
        //Navigate to next screen
    }
}