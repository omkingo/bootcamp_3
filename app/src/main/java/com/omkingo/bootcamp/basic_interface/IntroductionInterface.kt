package com.omkingo.bootcamp.basic_interface


interface IntroductionInterface {
    fun setContent(contentText: String)

    fun navigateNext()
}
