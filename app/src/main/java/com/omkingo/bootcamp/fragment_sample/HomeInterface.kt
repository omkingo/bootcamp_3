package com.omkingo.bootcamp.fragment_sample

import com.omkingo.bootcamp.fragment_sample.data.FragmentModel

interface HomeInterface {
    fun setView(fmList: List<FragmentModel>)
}
