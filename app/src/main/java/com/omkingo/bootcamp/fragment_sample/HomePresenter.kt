package com.omkingo.bootcamp.fragment_sample

import com.omkingo.bootcamp.basic_api.ui.BeerFragment
import com.omkingo.bootcamp.fragment_sample.data.FragmentModel
import com.omkingo.bootcamp.fragment_sample.ui.main.PlaceholderFragment
import com.omkingo.bootcamp.fragment_sample.ui.main.ProfileFragment

class HomePresenter (private val view: HomeInterface) {
    fun setup() {
        val fmList = listOf(
            FragmentModel("Home", PlaceholderFragment.newInstance(1)),
            FragmentModel("Profile", ProfileFragment.newInstance()),
            FragmentModel("Beer", BeerFragment.newInstance())
        )
        view.setView(fmList)
    }
}