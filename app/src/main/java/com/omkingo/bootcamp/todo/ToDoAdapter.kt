package com.omkingo.bootcamp.todo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.todo.model.ToDoModel
import java.util.ArrayList

class ToDoAdapter(private val listener: ToDoClickListener) : RecyclerView.Adapter<ToDoListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoListViewHolder {
        return ToDoListViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return todoList.count()
    }

    override fun onBindViewHolder(holder: ToDoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
    }

    private val todoList: ArrayList<ToDoModel> = arrayListOf()

    fun addListTask(taskModel: ToDoModel) {
        todoList.add(taskModel)
        notifyDataSetChanged()
    }
}

class ToDoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
) {
    private val taskName: TextView = itemView.findViewById(R.id.task)

    fun bind(model: ToDoModel, listener: ToDoClickListener) {
        taskName.text = model.taskName
        itemView.setOnClickListener {
            listener.onItemClick(taskName.text.toString())
        }

        itemView.visibility = if (model.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}

interface ToDoClickListener {
    fun onItemClick(taskName: String)
}